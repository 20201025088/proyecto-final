//crear una escena


//THREE
let escena =new THREE.Scene();
//crear camara
let camara= new THREE.PerspectiveCamera(100,window.innerWidth / window.innerHeight, 0.1, 10 );

//crear lienzo
let lienzo= new THREE.WebGLRenderer();
lienzo.setSize( 600, 500 );
document.body.appendChild(lienzo.domElement);

//crear geometria
let cubo= new THREE.CircleGeometry(  0.5, 4 );  
let material= new THREE.MeshBasicMaterial( { color: 0xFFFFFF } );


//Crear la malla de la escena 
let niCubo= new THREE.Mesh( cubo, material );
let doscubo= new THREE.Mesh( cubo, material );
let adoscubo= new THREE.Mesh( cubo, material );
let bdoscubo= new THREE.Mesh( cubo, material );
let cdoscubo= new THREE.Mesh( cubo, material );


escena.add(niCubo);

//agregar la malla 

escena.add( niCubo,doscubo,adoscubo,bdoscubo,cdoscubo);

camara.position.z = 5;


let animar = function(){
    requestAnimationFrame(animar);

    niCubo.position.x=5
    niCubo.position.y=2
    

    doscubo.position.x=1
    doscubo.position.y=-3

    adoscubo.position.x=0
    adoscubo.position.y=2

    bdoscubo.position.x=-4
    bdoscubo.position.y=1

    cdoscubo.position.x=-7
    cdoscubo.position.y=4



    niCubo.rotation.x= niCubo.rotation.x+0.04;
    niCubo.rotation.y= niCubo.rotation.y+0.04;
    
    doscubo.rotation.x= doscubo.rotation.x+0.03;
    doscubo.rotation.y= doscubo.rotation.y+0.03;
    
    adoscubo.rotation.x= adoscubo.rotation.x+0.04;
    adoscubo.rotation.y= adoscubo.rotation.y+0.04;

    bdoscubo.rotation.x= bdoscubo.rotation.x+0.03;
    bdoscubo.rotation.y= bdoscubo.rotation.y+0.03;

    cdoscubo.rotation.x= cdoscubo.rotation.x+0.04;
    cdoscubo.rotation.y= cdoscubo.rotation.y+0.04;

        
    lienzo.render(escena,camara);
}
animar();