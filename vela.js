//crear una escena


//THREE
let escena =new THREE.Scene();
//crear camara
let camara= new THREE.PerspectiveCamera(100,window.innerWidth / window.innerHeight, 0.1, 10 );

//crear lienzo
let lienzo= new THREE.WebGLRenderer();
lienzo.setSize( 800, 500 );
document.body.appendChild(lienzo.domElement);

//crear geometria
let cubo= new THREE.CircleGeometry(  0.5, 4 );  
let material= new THREE.MeshBasicMaterial( { color: 0xFFFFFF } );


//Crear la malla de la escena 
let niCubo= new THREE.Mesh( cubo, material );
let doscubo= new THREE.Mesh( cubo, material );
let adoscubo= new THREE.Mesh( cubo, material );
let bdoscubo= new THREE.Mesh( cubo, material );
let cdoscubo= new THREE.Mesh( cubo, material );
let ddoscubo= new THREE.Mesh( cubo, material );
let edoscubo= new THREE.Mesh( cubo, material );
let fdoscubo= new THREE.Mesh( cubo, material );
let gdoscubo= new THREE.Mesh( cubo, material );
let hdoscubo= new THREE.Mesh( cubo, material );


escena.add(niCubo);

//agregar la malla 

escena.add( niCubo,doscubo,adoscubo,bdoscubo,cdoscubo,ddoscubo,edoscubo,fdoscubo,gdoscubo,hdoscubo);

camara.position.z = 5;


let animar = function(){
    requestAnimationFrame(animar);

    niCubo.position.x=-8
    niCubo.position.y=0
    

    doscubo.position.x=-7
    doscubo.position.y=1

    adoscubo.position.x=0
    adoscubo.position.y=3

    bdoscubo.position.x=1
    bdoscubo.position.y=1

    cdoscubo.position.x=4
    cdoscubo.position.y=-2

    ddoscubo.position.x=3
    ddoscubo.position.y=-4

    edoscubo.position.x=-3
    edoscubo.position.y=-4

    fdoscubo.position.x=-2
    fdoscubo.position.y=5

    gdoscubo.position.x=4
    gdoscubo.position.y=2

    hdoscubo.position.x=8
    hdoscubo.position.y=2

    niCubo.rotation.x= niCubo.rotation.x+0.04;
    niCubo.rotation.y= niCubo.rotation.y+0.04;
    
    doscubo.rotation.x= doscubo.rotation.x+0.03;
    doscubo.rotation.y= doscubo.rotation.y+0.03;
    
    adoscubo.rotation.x= adoscubo.rotation.x+0.04;
    adoscubo.rotation.y= adoscubo.rotation.y+0.04;

    bdoscubo.rotation.x= bdoscubo.rotation.x+0.03;
    bdoscubo.rotation.y= bdoscubo.rotation.y+0.03;

    cdoscubo.rotation.x= cdoscubo.rotation.x+0.03;
    cdoscubo.rotation.y= cdoscubo.rotation.y+0.03;

    ddoscubo.rotation.x= ddoscubo.rotation.x+0.04;
    ddoscubo.rotation.y= ddoscubo.rotation.y+0.04;
    
    edoscubo.rotation.x= edoscubo.rotation.x+0.03;
    edoscubo.rotation.y= edoscubo.rotation.y+0.03;

    fdoscubo.rotation.x= fdoscubo.rotation.x+0.04;
    fdoscubo.rotation.y= fdoscubo.rotation.y+0.04;
        
    gdoscubo.rotation.x= gdoscubo.rotation.x+0.03;
    gdoscubo.rotation.y= gdoscubo.rotation.y+0.03;

    hdoscubo.rotation.x= fdoscubo.rotation.x+0.04;
    hdoscubo.rotation.y= fdoscubo.rotation.y+0.04;
        
        
    lienzo.render(escena,camara);
}
animar();